﻿using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class EfDbInitializer : IDbInitializer
    {
        private readonly DataContext _dbContext;

        public EfDbInitializer(DataContext dataContext)
        {
            _dbContext = dataContext;
        }

        public void InitializeDb()
        {
            _dbContext.Database.EnsureDeleted();
            _dbContext.Database.EnsureCreated();

            _dbContext.AddRange(FakeDataFactory.Employees);
            _dbContext.SaveChanges();

            _dbContext.AddRange(FakeDataFactory.Preferences);
            _dbContext.SaveChanges();

            _dbContext.AddRange(FakeDataFactory.Customers);
            _dbContext.SaveChanges();
        }
    }
}
