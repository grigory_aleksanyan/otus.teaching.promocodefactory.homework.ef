﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public interface IDbInitializer
    {
        public void InitializeDb();
    }
}
